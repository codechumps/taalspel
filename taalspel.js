AjaxHandler.GETRequest('lijst.txt', function(object) {
        toJSON(object);
        onLoad();
    },
    function(error) {
        console.log("error " + JSON.stringify(error));
    });

var chosenWord, tijd,
    score = 0,
    gestart = false,
    secTimer = 0,
    times = 0,
    highscore = 0,
    gestopt = false;

function onLoad() {
    var randomWord = randWrd();
    chosenWord = randomWord.split('');

    if ((parseInt(localStorage.getItem("Highscore")) === null) || (parseInt(localStorage.getItem("Highscore")) === "NaN")) {
        alert("no highscore yet");
    } else {
        setHighscore(localStorage.getItem("Highscore"));
    }

    var submitButton = document.getElementById("submitButton");
    submitButton.addEventListener('click', function() {
        interAction();
    });

    var startButton = document.getElementById("startButton");
    startButton.addEventListener('click', function() {
        document.getElementById("wordspace").style.visibility = "visible";
        tijd = setInterval(timer, 1000);
        gestart = true;
    });

    addBox();
    console.log("OnLoad: " + chosenWord);
}

function isCorrectGuess(results) {
    let guessedIt = true;
    for (result of results) {
        if (result != 'C') {
            guessedIt = false;
        }
    }
    return guessedIt;
}

function getInput() {
    let inputElements = document.getElementsByClassName("active");
    let inputArray = [];

    for (element of inputElements) {
        inputArray.push(element.value);
    }
    return inputArray;
}

function copyUppercase(array) {
    let UppercasedArray = [];
    for (let i = 0; i < array.length; i++) {
        UppercasedArray[i] = array[i].toUpperCase();
    }
    return UppercasedArray;
}
// resultArray values
// C = Correct
// I = Invalid Position
// W = Wrong
function compareInput(WordArray, InputArray) {
    let resultArray = [];
    let _inputArray = copyUppercase(InputArray);
    let _wordArray = copyUppercase(WordArray);
    //check if correct place
    for (let i = 0; i < InputArray.length; i++) {
        let guessedLetter = _inputArray[i];
        let correctLetter = _wordArray[i];
        if (guessedLetter == correctLetter) {
            resultArray[i] = "C";
            delete _wordArray[i];
        }
    }
    //check if letter is in word.
    for (let i = 0; i < InputArray.length; i++) {
        let guessedLetter = _inputArray[i];
        if (resultArray[i] != "C") {
            if (_wordArray.includes(guessedLetter)) {
                resultArray[i] = "I";
                let index = _wordArray.indexOf(guessedLetter);
                delete _wordArray[index];
            } else {
                resultArray[i] = "W";
            }
        }
    }
    return resultArray;
}

function addBox() {
    var inputField = document.getElementsByClassName("active");
    var tempInput = [];
    for (inPut of inputField) {
        tempInput.push(inPut);
    }
    for (x of tempInput) {
        x.classList.remove("active");
        x.setAttribute("value", x.value);
    }
    var letters = chosenWord;
    var innerHtml = document.getElementById("wordspace").innerHTML + "<br>";
    var el = document.getElementById("wordspace");
    for (let i = 0; i < letters.length; i++) {
        innerHtml += '<input type="text" class="inputLetter active" maxlength="1">';
    }
    el.innerHTML = innerHtml;
    var letterEl = document.getElementsByClassName("inputLetter");
    for (x of letterEl) {
        addKeyPress(x);
    }
    times += 1;
}

function addKeyPress(element) {
    element.addEventListener("keypress", function(event) {
        if (event.which == 13) {
            interAction()
        } else if (event.which == 8) {
            if (this.previousElementSibling != null) {
                this.previousElementSibling.focus();
            }
        } else {
            if (this.nextElementSibling != null) {
                this.nextElementSibling.focus();
            }
        }
    }, false);
}

function checkResult(chdArray) {
    var inputField = document.getElementsByClassName("active");
    for (let i = 0; i < chdArray.length; i++) {
        if (chdArray[i].toUpperCase() == "C") {
            score += 20;
            inputField[i].classList.add("correct");
        } else if (chdArray[i].toUpperCase() == "I") {
            score += 5;
            inputField[i].classList.add("wrongPlace");
        } else {
            inputField[i].classList.add("incorrect");
        }
    }
    updateScore(score);
}

function interAction() {
    if (gestart && !gestopt) {
        let deActivate = document.getElementsByClassName("active");
        for (x of deActivate) {
            x.disabled = true;
        }
        if (times < 6 && !gestopt) {
            let inPut = getInput();
            let resultArray = compareInput(chosenWord, inPut);
            checkResult(resultArray);
            if (!isCorrectGuess(resultArray)) {
                if (times != 5) {
                    addBox();
                } else {
                    alert("Je kunt niet meer spelen,\nHet spel is gestopt!\nTijd: " + pad(parseInt(secTimer / 60)) + ":" + pad(secTimer % 60) + "\nPunten: " + score);
                    setHighscore(score);
                    updateScore(score);
                    document.getElementById("submitButton").innerHTML = "Opnieuw";
                    gestopt = true;
                    clearInterval(tijd);
                }
            } else {
                var temp = scoreCalc(secTimer);
                score = temp + 1000;
                setHighscore(score);
                updateScore(score);
                alert("Geraden\nHet duurde: " + pad(parseInt(secTimer / 60)) + ":" + pad(secTimer % 60) + "\nPunten: " + score);
                document.getElementById("submitButton").innerHTML = "Opnieuw";
                gestopt = true;
                clearInterval(tijd);
            }
        }
    } else if (gestopt) {
        score = 0;
        gestart = false;
        secTimer = 0;
        times = 0;
        highscore = 0;
        gestopt = false;
        var clearBrArray = document.getElementsByTagName("br");
        var clrBrArr = [];
        var clearInputArray = document.getElementsByTagName("input");
        var clrInpArr = [];

        for (var y = 0; y < clearInputArray.length; y++) {
            clrInpArr.push(clearInputArray[y]);
        }
        for (var i = 0; i < clrInpArr.length; i++) {
            clrInpArr[i].remove();
        }
        for (var t = 0; t < clearBrArray.length; t++) {
            clrBrArr.push(clearBrArray[t]);
        }
        for (var w = 0; w < clrBrArr.length; w++) {
            clrBrArr[w].remove();
        }

        chosenWord = randWrd().split('');
        console.log("Nieuwe game: " + chosenWord);
        document.getElementById("wordspace").style.visibility = "hidden";
        document.getElementById("submitButton").innerHTML = "Check";
        addBox();

    } else {
        alert("Start eerst het spel!");
    }
}

function timer() {
    // ++secTimer
    // var time = pad(parseInt(secTimer / 60)) + ":" + pad(secTimer % 60);
    // document.getElementById("timer").innerHTML = time;
}

function pad(val) {
    var valString = val + "";
    if (valString.length < 2) {
        return "0" + valString;
    } else {
        return valString;
    }
}

function scoreCalc(totalTime) {
    var penalty;
    if (totalTime = 1) {
        penalty = -0.95;
    } else if (totalTime = 2) {
        penalty = -0.93;
    } else if (totalTime = 3) {
        penalty = -0.9;
    } else if (totalTime = 4) {
        penalty = -0.875;
    } else if (totalTime = 5) {
        penalty = -0.8;
    } else if (totalTime = 6) {
        penalty = -0.725;
    } else if (totalTime = 7) {
        penalty = -0.65;
    } else if (totalTime = 8) {
        penalty = -0.6;
    } else if (totalTime = 9) {
        penalty = -0.5;
    } else if (totalTime = 10) {
        penalty = -0.4;
    } else if ((totalTime > 10) && (totalTime <= 30)) {
        penalty = 0;
    } else if ((totalTime > 30) && (totalTime <= 40)) {
        penalty = 0.29;
    } else if ((totalTime > 40) && (totalTime <= 50)) {
        penalty = 0.58;
    } else if ((totalTime > 50) && (totalTime <= 60)) {
        penalty = 0.87;
    } else {
        penalty = 1.3;
    }

    score = parseInt(score / ((times + penalty) / 2));
    return score;
}

function updateScore(tempScore) {
    if (tempScore.toString().length <= 0) {
        tempScore = "0000" + tempScore;
    } else if (tempScore.toString().length < 1) {
        tempScore = "0000" + tempScore;
    } else if (tempScore.toString().length < 2) {
        tempScore = "000" + tempScore;
    } else if (tempScore.toString().length < 3) {
        tempScore = "00" + tempScore;
    } else if (tempScore.toString().length < 4) {
        tempScore = "0" + tempScore;
    }
    document.getElementById("score1").innerHTML = tempScore;
}

function setHighscore(checkScore) {
    if (localStorage.getItem("Highscore") == null) {
        localStorage.setItem("Highscore", checkScore);
    } else if (checkScore > parseInt(localStorage.getItem("Highscore"))) {
        localStorage.setItem("Highscore", checkScore);
    }
    dispHighscore(checkScore);
}

function dispHighscore(hs) {
    document.getElementById("highscore").innerHTML = hs;
}