var AjaxHandler = {
    xhttp: null,
    GETRequest: function (url,onSuccess,onError)
    {
        var xhttp = new XMLHttpRequest();
        
        xhttp.onreadystatechange = function (){
            if(xhttp.readyState == 4) {
                if(xhttp.status == 200 || xhttp.status == 206){
                    if(onSuccess != null) {
                        onSuccess(xhttp.responseText);
                    }
                }else{
                    if(onError != null) {
                        onError(xhttp.responseText);
                    }
                }
                
            }
        }
        xhttp.open('GET',url,true);
        xhttp.responseType = "text";
        xhttp.send();
    },
    POSTRequest: function(url,onSuccess,onError){
        this.xhttp = new XMLHttpRequest();
        
        this.xhttp.onreadystatechange = function (){
            if(this.readyState == 4) {
                if(this.status == 200){
                    if(onSucces != null) {
                        onSuccess(this.responseText);
                    }
                }else{
                    if(onError != null) {
                        onError(this.responseText);
                    }
                }
                
            }
        }

        this.xhttp.open('GET',url);
        this.xhttp.setRequestHeader('');
        this.xhttp.send();
    }
}